import React from 'react';

const DivideIndiv = props => {

    return (
        <div>
            {props.people.map((person, i)=>(
                <div key = {i}>
                    <input type='text' value={person.name} placeholder="Имя" onChange={(e)=>props.onChangeBill(e, 'name', i)}/>
                    <input type='text' value={person.price} placeholder="Сумма" onChange={(e)=>props.onChangeBill(e, 'price', i)}/>
                    <button onClick={()=>props.deletePerson(i)}>-</button>
                </div>
            ))}
            <button onClick={props.appPeople}>+</button>

            <div>Чаевые: <input type='text' value={props.tip} placeholder="Кол-во человек" onChange={(e)=>props.onChangeBill(e, 'tip')}/> %</div>
            <div>Доставка: <input type='text' value={props.delivery} placeholder="Кол-во человек" onChange={(e)=>props.onChangeBill(e, 'delivery')}/> сом</div>
        </div>
    );
};

export default DivideIndiv;