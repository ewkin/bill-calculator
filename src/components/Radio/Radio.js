import React from 'react';

const Radio = ({onRadioChange, mode}) => {

    return (
        <div>
            <p><input type='radio'
                      name='options'
                      value='first'
                      checked={mode==='first'}
                      onChange={onRadioChange}/>Поровну между всеми участниками</p>
            <p><input type='radio'
                      name='options'
                      value='second'
                      checked={mode==='second'}
                      onChange={onRadioChange}/>Каждому индивидуально</p>
        </div>
    );
};

export default Radio;