import React from 'react';

const Total = props => {
    if(typeof props.people ==='object'){
        return (
            <div>
                <p>Общая сумма: {props.total} сом</p>
                {props.people.map((item, i)=>(
                    <p key ={i}>{item.name} = {item.pay}</p>
                ))}
            </div>
        )

    } else {
        return (
            <div>
                <p>Общая сумма: {props.total} сом</p>
                <p>Количество человек: {props.people} </p>
                <p>Каждый платит по : {props.each}  сом</p>
            </div>
    );
    }
};

export default Total;