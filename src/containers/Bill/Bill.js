import React, {useState} from 'react';
import './Bill.css'
import Radio from "../../components/Radio/Radio";
import DivideEqual from "../../components/DivideEqual/DivideEqual";
import Total from "../../components/Total/Total";
import DivideIndiv from "../../components/DivideIndiv/DivideIndiv";

const Bill = () => {
    const [mode, setMode] = useState('first');
    const [bill, setBill] = useState({
        people:0,
        amount:0,
        tip:0,
        delivery:0,

    });
    const [showTotal, setShowTotal] = useState(false);
    const [total, setTotal] = useState({totalPrice:0, each:0});
    const [people, setPeople] = useState([{name:'', price:'', pay:0}]);

    const addPeople =()=>{
        setPeople([...people,{name: '', price:'', pay:0}]);
    };


    const onRadioChange = e =>{
        setMode(e.target.value)
        setShowTotal(!total);
        setBill({...bill, 'people':0, 'amount':0, 'tip': 0, 'delivery': 0});
        setTotal({...total, 'totalPrice':0, 'each':0});

    };

    const onChangeBill =(e, name, i) =>{
        if(name !=='name' && isNaN(e.target.value)) return
        setBill({...bill, [name]: e.target.value})
        if(typeof i !=='undefined'){
            const peopleCopy = [...people];
            const personCopy = peopleCopy[i];
            personCopy[name] = e.target.value;
            peopleCopy[i]=personCopy;
            setPeople(peopleCopy);
        };
    };
    const deletePerson = (i) =>{
        const peopleCopy = [...people];
        peopleCopy.splice(i,1);
        setPeople(peopleCopy);
    }

    let totalAmount = null;
    const calculate = () =>{
        let totalPay=0;
        if(mode==='first'){
            if(parseInt(bill.people)>0){
                setTotal({...total, 'totalPrice': parseInt(bill.amount)*parseInt(bill.tip)/100+parseInt(bill.amount)+parseInt(bill.delivery),
                    'each': Math.ceil((parseInt(bill.amount)*parseInt(bill.tip)/100+parseInt(bill.amount)+parseInt(bill.delivery))/parseInt(bill.people))});
                if(!showTotal){setShowTotal(!showTotal)}
            }
        } else {
            people.map((item, i)=>{
             let total = Math.ceil(parseInt(item.price)*parseInt(bill.tip)/100+parseInt(item.price)+parseInt(bill.delivery)/people.length);
             totalPay +=total;
             setTotal({...total, 'totalPrice':totalPay});
             const peopleCopy = [...people];
             const personCopy = peopleCopy[i];
             personCopy.pay = total;
             peopleCopy[i]=personCopy;
             setPeople(peopleCopy);
             return null
            })
            if(!showTotal){setShowTotal(!showTotal)}
        }
    };
    if(showTotal){
        if(mode==='first'){
            totalAmount = <Total total = {total.totalPrice} each = {total.each} people = {bill.people}/>
        } else {
            totalAmount = <Total total = {total.totalPrice} people={people} />

        }
    }
    return (
        <div className="container">
            Сумма заказа считаетается:
            <Radio
                onRadioChange={onRadioChange}
                mode = {mode}
            />
            {mode==='first' &&(
                <div>
                    <DivideEqual onChangeBill={onChangeBill} bill={bill}/>
                </div>
            )}
            {mode==='second' &&(
                <div>
                    <DivideIndiv people={people} tip={bill.tip} delivery={bill.delivery} deletePerson={deletePerson} onChangeBill={onChangeBill} appPeople={addPeople}/>
                </div>
            )}
            <button onClick={()=> calculate()}>Calculate</button>
            {totalAmount}
        </div>
    );
};

export default Bill;